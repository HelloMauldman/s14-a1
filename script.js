// Variables and data types

// let name = "Zuitt"; //string
// let name2 = 'Coding Bootcamp';
// let score = 34; //number
// let score2 = "17";

// console.log(name2);
// console.log(name + " " + name2);

// const legalAge = 18;



// Functions

function printStars(){
	console.log(" * ");
	console.log(" ** ");
	console.log(" *** ");
	console.log(" **** ");
};

// printStars();
// printStars();
// printStars();
// printStars();

function sayHello(){
	console.log("Hello!");
}

// Function with one parameter

function sayHello2(name){
	console.log("Hello " + name);

}
// sayHello2("Zuitt");
// sayHello2("Shinji");

function sayHello3(x,y){
	console.log("Hello " + x + " " + y)
}

sayHello3("Bruce", "Wayne")

// Create a functino called addNum that accepts two numebrs and prins in teh console the sum.

function addNum(x,y){
	console.log( x + y);
}

addNum(5,5);